FROM openjdk:8-jre-alpine
COPY target/assignment-*.jar /assignment.jar
ENV profile "h2"
CMD ["java", "-jar", "/assignment.jar"]
